
// Опции счетчика
var options = {
    useEasing : true,
    useGrouping : true,
    separator : '',
    decimal : '',
};


$(document).ready(function(){

  // Анимация контента блока с перечислением предоставляемых услуг
  var screenWidth = $(window).width();

  if (screenWidth>1024) {
    var scene = $('#scene').get(0)
    var parallax = new Parallax(scene);
  }

  // Анимация контента страницы
  window.sr = ScrollReveal();
    sr.reveal(document.querySelectorAll('.header_column'), { duration: 1000}, 200);
    sr.reveal(document.querySelectorAll('.menu_list li'), { duration: 1000}, 200);
    sr.reveal(document.querySelectorAll('.slick-dots li'), { duration: 1000}, 200);



    // Анимирую контент главного слайдера

    // sr.reveal(document.querySelectorAll('.slick-current .slide_category'));
    // sr.reveal(document.querySelectorAll('.slick-current .slide_title'));
    // sr.reveal(document.querySelectorAll('.slick-current .slide_text p'));

    $('.main_slider').init(function(slick){

      var actualSlide = $('.slick-current').find('.slide_title')
      var actualText = $('.slick-current').find('.slide_text p')
      var actualCat = $('.slick-current').find('.slide_category')

      TweenMax.fromTo (actualSlide, 0.5, {
        autoAlpha: 0
      }, {
        autoAlpha: 1
      })
      TweenMax.fromTo (actualText, 0.5, {
        autoAlpha: 0
      }, {
        autoAlpha: 1
      })
      TweenMax.fromTo (actualCat, 0.5, {
        autoAlpha: 0
      }, {
        autoAlpha: 1
      })
    })

    $('.main_slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){


      var actualSlide = $(slick.$slides[currentSlide]).find('.slide_title')
      var actualText = $(slick.$slides[currentSlide]).find('.slide_text p')
      var actualCat = $(slick.$slides[currentSlide]).find('.slide_category')

      TweenMax.fromTo (actualSlide, 0.5, {
        autoAlpha: 1,
        scale: 1
      }, {
        autoAlpha: 0,
        scale: 1.3
      })
      TweenMax.fromTo (actualText, 0.5, {
        autoAlpha: 1,
        scale: 1
      }, {
        autoAlpha: 0,
        scale: 1.3
      })
      TweenMax.fromTo (actualCat, 0.5, {
        autoAlpha: 1,
        scale: 1
      }, {
        autoAlpha: 0,
        scale: 1.3
      })
    })


    $('.main_slider').on('afterChange', function(event, slick, currentSlide, nextSlide){

      var actualSlide = $(slick.$slides[currentSlide]).find('.slide_title')
      var actualText = $(slick.$slides[currentSlide]).find('.slide_text p')
      var actualCat = $(slick.$slides[currentSlide]).find('.slide_category')


      TweenMax.fromTo (actualSlide, 0.5, {
        autoAlpha: 0,
        scale: .7
      }, {
        autoAlpha: 1,
        scale: 1
      })
      TweenMax.fromTo (actualText, 0.5, {
        autoAlpha: 0,
        scale: .7
      }, {
        autoAlpha: 1,
        scale: 1
      })
      TweenMax.fromTo (actualCat, 0.5, {
        autoAlpha: 0,
        scale: .7
      }, {
        autoAlpha: 1,
        scale: 1
      })



      // sr.reveal(document.querySelectorAll('.slick-current .slide_category'));
      // sr.reveal(document.querySelectorAll('.slick-current .slide_title'));
      // sr.reveal(document.querySelectorAll('.slick-current .slide_text p'));

    });



    $(".main_slider ").on("afterChange", function (){
      // sr.reveal(document.querySelectorAll('.slick-current .slide_category'));
      // sr.reveal(document.querySelectorAll('.slick-current .slide_title'));
      // sr.reveal(document.querySelectorAll('.slick-current .slide_text p'));
    });

    // Анимирую контент секции преимуществ
    sr.reveal(document.querySelectorAll('.advantages_section .page_title'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.advantages_col'), {
      duration: 1000,
      viewOffset: { bottom: 100 },
    }, 200);

    // Анимирую контент секции бонусов
    sr.reveal(document.querySelectorAll('.bonuses_section .page_title'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.bonuses_section_col'), {
      viewOffset: { bottom: 100 },
      duration: 1000,
      beforeReveal: function(dom){
        startCounter(options, dom);
      }
    }, 200);

    // Анимирую контент секции возможностей
    sr.reveal(document.querySelectorAll('.abilities_section .page_title'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.abilities_section .preview_col'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.abilities_description_item.first_item'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.abilities_description_item.second_item'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.abilities_description_item.third_item'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.abilities_description_item.fourth_item'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.abilities_description_item.fifth_item'), { viewOffset: { bottom: 100 } });

    // Анимирую контент секции визуализации
    sr.reveal(document.querySelectorAll('.visualize_section .page_title'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.visualize_pic_container'), {
      viewOffset: { bottom: 100 },
      beforeReveal: function(){
        animateSvg()
      }
    });
    sr.reveal(document.querySelectorAll('.countdown_container'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.visualize_container .order_form'), { viewOffset: { bottom: 100 } });

    // Анимирую контент секции отличий
    sr.reveal(document.querySelectorAll('.differences_section .page_title'));
    sr.reveal(document.querySelectorAll('.differences_row.first_row .differences_block_left'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.differences_row.first_row .differences_block_right'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.differences_row.second_row .differences_block_left'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.differences_row.second_row .differences_block_right'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.differences_row.third_row .differences_block_left'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.differences_row.third_row .differences_block_right'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.differences_row.fourth_row .differences_block_left'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.differences_row.fourth_row .differences_block_right'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.differences_row.fifth_row .differences_block_left'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.differences_row.fifth_row .differences_block_right'), {
      viewOffset: { bottom: 100 },
      duration: 1000,
      afterReveal: function(){
        animateSvg()
      }
    }, 200);

    // Анимирую контент секции квалификации
    sr.reveal(document.querySelectorAll('.background-grey .page_title'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.qualities_description_col'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);

    // Анимирую контент секции спецификации
    sr.reveal(document.querySelectorAll('.specifics_section .page_title'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.specifics_box'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);

    // Анимирую контент секции бесплатных опций
    sr.reveal(document.querySelectorAll('.free_stuff_section .page_title'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.free_stuff_col'), {
      viewOffset: { bottom: 100 },
      duration: 1000,
      beforeReveal: function(dom){
        freeStuffAnimation(dom);
      }
    }, 200);
    sr.reveal(document.querySelectorAll('.free_stuff_section .place_order'), { viewOffset: { bottom: 100 } });

    // Анимирую контент секции статистики
    sr.reveal(document.querySelectorAll('.stats_section .page_title'), {
      viewOffset: { bottom: 100 },
      beforeReveal: function(){
        animateSvg()
      }
    });
    sr.reveal(document.querySelectorAll('.stats_section .stats_col'), {
      viewOffset: { bottom: 100 },
      duration: 1000,
      beforeReveal: function(dom){
        startStatsCounter(options, dom)
      },
      afterReveal: function(dom) {
        showPlus(dom)
      }

    }, 200);

    // Анимирую контент включенных опций
    sr.reveal(document.querySelectorAll('.included_section .page_title'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.included_section .included_col.first_col'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.included_section .included_col.second_col'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.included_section .included_col.third_col'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.included_section .included_col.fourth_col'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.included_section .included_col.fifth_col'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);
    sr.reveal(document.querySelectorAll('.included_section .included_col.last_col'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);

    // Анимирую контент призывающего контейнера
    sr.reveal(document.querySelectorAll('.chances_section .chances_span'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.chances_section .chances_span.bold_span'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.chances_col_content_phone '), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.chances_phone_col '), { viewOffset: { bottom: 100 } });

    // Анимирую контент контейнера с портфолио
    sr.reveal(document.querySelectorAll('.portfolio_section .page_title'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.portfolio_tabs'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.grid-item'), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);

    // Анимирую контент контейнера с партнерами
    sr.reveal(document.querySelectorAll('.partners_section .page_title'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.partner_item '), {
      viewOffset: { bottom: 100 },
      duration: 1000
    }, 200);

    // Анимирую контент контейнера с советами
    sr.reveal(document.querySelectorAll('.advices_section .advices_col'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.advices_bill_list li'), { viewOffset: { bottom: 100 } });

    // Анимирую контент контейнера с контактами
    sr.reveal(document.querySelectorAll('.feedback_title'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.feedback_form input'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.feedback_form button'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.feedback_data_title'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.feedback_data_item'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.socials_container .social_item'), { viewOffset: { bottom: 100 } });


    // Анимирую контент футера
    sr.reveal(document.querySelectorAll('.footer_section .page_title'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.footer_section .mail_contact'), { viewOffset: { bottom: 100 } });
    sr.reveal(document.querySelectorAll('.footer_section .phone_contact'), { viewOffset: { bottom: 100 } });

});
