function WBSGalleryPlugin(tag, options) {

    var options = options || {};

    var animationType = options.animationType ? options.animationType : 'default' ;    
    var $parent = $(tag);
    var $device = $parent.find('.device')
    var $deviceImgs = $parent.find('.thumbs-container img')
    var $thumbItems = $parent.find('.thumb__wrapper')
    var zIndex = 100;
    
    
    
    $thumbItems.hover(changeImage, function () {
        return false
    })
    
    function changeImage(e) {
        if ($(this).hasClass('active')) {
            return false
        }
        
        $(this)
        .addClass('active')
        .siblings()
        .removeClass('active')
        
        var index = $(this).index()

        if (animationType == 'edges') {
            initEdgeAnimation($(this), e, index, 'edges')
        } else {
            initEdgeAnimation($(this), e, index, 'default')
        }
    }

    function initEdgeAnimation(elm, e, index, type) {
        var clonedElm;

        var edge = closestEdge(
            e.offsetX,
            e.offsetY,
            elm[0].getBoundingClientRect().width,
            elm[0].getBoundingClientRect().height
        );


        zIndex++;

        if (type == 'edges') {
            switch (edge) {
                case 'top':
                    startAnimation('from-top');
                    break;
                case 'right':
                    startAnimation('from-right');
                    break;
                case 'bottom':
                    startAnimation('from-bottom');
                    break;
                case 'left':
                    startAnimation('from-left');
                    break;

                default:
                    break;
            }
        } else {
            startAnimation('from-left');
        }
        

        function startAnimation(cls) {
            clonedElm = $deviceImgs
                .eq(index)
                .clone(true)
                .addClass('cloned')
                .css({
                    'z-index': zIndex
                })

            $deviceImgs
                .parent()
                .prepend(clonedElm)

            clonedElm
                .addClass(cls)
                .on('animationend', clearAttrs)
        }


        function clearAttrs() {
            clonedElm.remove()
            $deviceImgs
                .removeClass('active')
                .eq(index)
                .addClass('active')
        }
    }



    function closestEdge(x, y, w, h) {
       var x2 = x + w;
       var y2 = y + h;
       var left = 100, right = 100, top = 100, bottom = 100;
       var fromLeft, fromRight, fromBottom, fromTop;
       if (x < w/2) {
           left = x
       }
       if (x > w/2 ) {
           right = (x - w) * -1
       }
       if (y < h/2) {
           top = y
       }
       if (y > h/2) {
           bottom = (y - h) * -1
       }

    
       if (left < top && left < bottom) {
           return 'left'
       }
       if (right < top && right < bottom) {
           return 'right'
       }
       if (top < left && top < right) {
           return 'top'
       }
       if (bottom < left && bottom < right) {
           return 'bottom'
       }
    }

}