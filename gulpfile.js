var gulp = require('gulp')
var express = require('express');
var app     = express();
var pug		= require('gulp-pug');
var path		= require('path');
var watch		= require('gulp-watch');
var sass		= require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var clean		= require('gulp-clean');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('serve', function() {

	app.set('view engine', 'pug');

	app.set('views', path.join(__dirname, '/dev'))

	app.use(express.static(__dirname + '/build'))

	app.get('/:page?', function(req, res) {
		if (req.url == '/favicon.ico') {
			return false
		}


		var fileName = req.url.replace(/\//g, '') || 'index'
		if (fileName == 'home' || fileName == 'index') {
			res.render('index')
		} else {
			res.render(fileName)
		}

	})


	var listener    = app.listen(2999);
	var port        = listener.address().port;
	// proxy на локальный сервер на Express
	browserSync.init({
	  proxy: 'http://localhost:'   + port,
	  // startPath: '/build/',
	  notify: false,
	  tunnel: false,
	  host: 'localhost',
	  port: port,
	  logPrefix: 'Proxy to localhost:' + port,
	});
	// обновляем страницу, если был изменен исходник шаблона
	browserSync.watch('./dev/*.pug').on('change', browserSync.reload);
	// browserSync.watch('./**/*.js').on('change', browserSync.reload);
	browserSync.watch('./dev/blocks/**/*.html').on('change', browserSync.reload);
})

gulp.task('html', function () {
	return gulp.src('dev/*.pug')
		.pipe(pug({
			pretty: true
		}))
		.pipe(gulp.dest('build/'))
})

gulp.task('scss', function(){
	return gulp.src('dev/scss/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
            browsers: ['last 12 versions'],
            cascade: false
        }))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('build/css/'))
		.pipe(browserSync.reload({stream:true}))
})

gulp.task('cleanHtml', function () {
	return gulp.src('build/*.html')
		.pipe(clean());
})


gulp.task('moveHtml', function () {
	return gulp.src('dev/*.html')
		.pipe(gulp.dest('build'))
})


gulp.task('moveJs', function () {
	return gulp.src(['dev/js/**/*.js'])
		.pipe(gulp.dest('build/js'))
		.pipe(browserSync.reload({stream:true}))
})


//
// gulp.task('browser-sync', function() {
//     browserSync.init({
//         server: {
//             baseDir: "./build/"
//         },
// 				notify: false
//     });
//
// 	gulp.watch("build/*.html").on('change', browserSync.reload);
// 	gulp.watch("build/js/*.js").on('change', browserSync.reload);
// });


gulp.task('watch', function() {
	gulp.watch('dev/scss/**/*.scss', ['scss'], browserSync.reload)
	gulp.watch('dev/js/**/*.js', ['moveJs'])
	gulp.watch('dev/*.html', ['moveHtml'])
})

gulp.task('default', ['cleanHtml', 'serve', 'moveJs', 'scss', 'watch'])
// gulp.task('default', [ 'moveHtml', 'moveJs', 'scss', 'browser-sync', 'watch'])
